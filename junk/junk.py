class TestPanel(nukescripts.PythonPanel):

    def __init__(self):
        nukescripts.PythonPanel.__init__(self, 'Test')

p = TestPanel()

k = nuke.Link_Knob('test')
k.setLink('root.HubRead44.file')
p.addKnob(k)
p.showModalDialog()


# As a more generic answer, though, if you want to copy any kind of knob value
# (single value, animation, expression...) across between two nodes, your best
# bet is probably to use the fromScript() and toScript() knob methods.
# Something like this should let you easily copy all knobs that exist in both
# nodes, while excluding some of them as needed (name, xpos, ypos, etc...)
def copyKnobValues(sourceNode, destNode, excludeKnobs=[]):
    sourceKnobs = sourceNode.knobs()
    destKnobs = destNode.knobs()
    intersection = dict([(item, sourceKnobs[item]) for item in
                         sourceKnobs.keys() if item not in excludeKnobs and destKnobs.has_key(item)])
    for knob in intersection.keys():
        destNode[knob].fromScript(sourceNode[knob].toScript())

            # http://www.nukepedia.com/python/nodegraph/replacenodes
            self.addKnob(self._masterKnobs[knob])
            nuke.Enumeration_Knob('nodes', 'Source Nodes', ['all', 'selected'])


c = 1
for n in nuke.toNode('Group1').nodes():
    if n.Class() == 'Input':
        print n.name()
        print c
        nn = 'InputR' +`c`
        n['name'].setValue(nn)
        c += 1


nuke.INPUTS


s = nuke.selectedNodes()
for n in s:
    print n.name()

nuke.Group.nodes(nuke.toNode('TurntableDaily'))
nuke.Group.nodes(nuke.toNode('root'))
nuke.Group.minimumInputs(nuke.toNode('TurntableDaily'))
nuke.Group.input(nuke.toNode('TurntableDaily'), 0).name()

td = nuke.toNode('TurntableDaily')
for n in td.selectedNodes():
    print 'RefFrame' + n.name()[1:]
    n['name'].setValue('RefFrame' + n.name()[1:])

        def printKnobValue():
            k = nuke.thisKnob()
            print k.name()

        nuke.addKnobChanged(printKnobValue, nodeClass='Group')
        nuke.removeKnobChanged(printKnobValue)
        nuke.toNode('TurntableDaily').knob('knobChanged')

        nuke.toNode('TurntableDaily')['knobChanged'].getValue()
        # Result:
knb = nuke.thisKnob().name()
if knb == "slateDuration" or knb == "referenceFrameDuration" or knb == "clipDuration":
slateDur = nuke.thisNode()['slateDuration'].getValue()
refDur = nuke.thisNode()['referenceFrameDuration'].getValue()
clipDur = nuke.thisNode()['clipDuration'].getValue()
nuke.root().knob('first_frame').setValue(1.0 - slateDur)
nuke.root().knob('last_frame').setValue(clipDur + refDur)
print 'knob changed - slate: ' + `slateDur` + ' - shot: ' + `shotDur` + ' - ref: ' + `refDur`

td = nuke.toNode('TurntableDaily')

td['knobChanged'].getValue()

td['updateUI'].setValue("""

td = nuke.toNode('TurntableDaily')

slateDur= td['slateDuration'].getValue()
refDur= td['referenceFrameDuration'].getValue()
clipDur = td['clipDuration'].getValue()
nuke.root().knob('first_frame').setValue(1.0 - slateDur)
nuke.root().knob('last_frame').setValue(clipDur+refDur)
# print 'knob changed - slate: ' + `slateDur` +' - clip: '+ clipDur +' - ref: '+ `refDur`
""")

knb = nuke.thisKnob().name()
if knb == 'slateDuration' or knb == 'referenceFrameDuration' or knb == 'clipDuration':
    td = nuke.thisNode()
    slateDur = td['slateDuration'].getValue()
    refDur = td['referenceFrameDuration'].getValue()
    clipDur = td['clipDuration'].getValue()
    nuke.root().knob('first_frame').setValue(1.0)
    nuke.root().knob('last_frame').setValue(slateDur + clipDur + refDur)


val = nuke.toNode("HubRead44").knob('scene').value()
nuke.toNode("HubRead44").knob('scene').setValue('uk001a_Welcome_Master')

nuke.toNode("HubRead44").knob('scene').setValue('swe001_Welcome_Master')
nuke.toNode("HubRead64").knob('scene').setValue('swe001_Welcome_Master')

nuke.toNode("HubRead44").knob('scene').setValue('uk001a_Welcome_Master')
nuke.toNode("HubRead64").knob('scene').setValue('uk001a_Welcome_Master')

nuke.toNode("HubRead44").metadata()

nuke.knobChanged()


nuke.toNode("HubRead78").knob('scene').setValue('swe001_Welcome_Master')
nuke.toNode("HubRead78").knob('shot').setValue('swe001_070')
nuke.toNode("HubRead78").knob('layer').setValue('E3D_swe001_070_TREE_MATTE')
nuke.toNode("HubRead78").knob('layerVersion').setValue('1')
nuke.toNode("HubRead78").knob('pass').setValue('beauty')

nuke.toNode("HubRead56").knob('scene').setValue('swe001_Welcome_Master')
nuke.toNode("HubRead56").knob('shot').setValue('swe001_070')
nuke.toNode("HubRead56").knob('layer').setValue('E3D_swe001_070_streetLights')
nuke.toNode("HubRead56").knob('layerVersion').setValue('1')
nuke.toNode("HubRead56").knob('pass').setValue('beauty')

NODE:
    HubRead78:
        Set scene to swe001_Welcome_Master
NODE:
    HubRead78:
        Set shot to swe001_070
NODE:
    HubRead78:
        Set layer to E3D_swe001_070_TREE_MATTE
NODE:
    HubRead78:
        Set layerVersion to 1
NODE:
    HubRead78:
        Set pass to beauty

NODE:
    HubRead56:
        Set scene to swe001_Welcome_Master
NODE:
    HubRead56:
        Set shot to swe001_070
NODE:
    HubRead56:
        Set layer to E3D_swe001_070_streetLights
NODE:
    HubRead56:
        Set layerVersion to 1
NODE:
    HubRead56:
        Set pass to beauty
