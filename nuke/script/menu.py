import kine.propagate_knobs as ck
import kine.propagate_hub_scene as hs

def launchPropagateKnobsPanel():
    '''Run the panel script and add it as a tab into the pane it is called from'''
    panel = ck.PropagateKnobsPanel()
    # panel.show()

def launchPropagateHubScenePanel():
    '''Run the panel script and add it as a tab into the pane it is called from'''
    panel = hs.PropagateHubScenePanel()
    # panel.show()

props = nuke.menu("Properties")
props.addCommand("Propagate Knobs", launchPropagateKnobsPanel)
props.addCommand("Propagate Hub Scene", launchPropagateHubScenePanel)
