import nuke

ALLOWABLE_TYPES = ["Enumeration_Knob", "File_Knob", "Boolean_Knob",
                   "String_Knob", "Int_Knob", "Format_Knob",
                   "Float_Knob"]

ENUM_POLICY = ["Don't Propagate", "Set to Closest", "Force New Value"]

VERBOSITY_LEVELS = ["None", "Summary", "Full"]

PROPAGATION_SCOPE = ["Selected", "All"]


def get_source_node():
    """this_is_a_one_liner."""
    panel_nodes = nuke.openPanels()
    if panel_nodes:
        return nuke.toNode(panel_nodes[-1])
    else:
        raise ValueError

