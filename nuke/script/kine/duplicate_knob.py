"""this_is_a_one_liner."""
import nuke


def explicit_label(knob):
    """this_is_a_one_liner."""
    result = ""
    if knob.label():
        result = "%s [%s]" % (knob.label(), knob.name())
    else:
        result = "[%s]" % (knob.name())
    return result
    # return result.rjust(40)


def duplicate_enumeration_knob(knob, label=None):
    """duplicate an enumeration knob."""
    if label == None:
        label = explicit_label(knob)
    new_knob = nuke.Enumeration_Knob(
        knob.name(),
        label,
        knob.values())

    new_knob.fromScript(knob.toScript())

    return new_knob


def duplicate_file_knob(knob, label=None):
    """this_is_a_one_liner."""
    if label == None:
        label = explicit_label(knob)
    new_knob = nuke.File_Knob(knob.name(), label)
    new_knob.fromScript(knob.toScript())

    return new_knob


def duplicate_boolean_knob(knob, label=None):
    """this_is_a_one_liner."""
    if label == None:
        label = explicit_label(knob)
    new_knob = nuke.Boolean_Knob(knob.name(), label)
    new_knob.fromScript(knob.toScript())

    return new_knob


def duplicate_string_knob(knob, label=None):
    """this_is_a_one_liner."""
    if label == None:
        label = explicit_label(knob)
    new_knob = nuke.String_Knob(knob.name(), label)
    new_knob.fromScript(knob.toScript())

    return new_knob


def duplicate_text_knob(knob, label=None):
    """this_is_a_one_liner."""
    if label == None:
        label = explicit_label(knob)    
    new_knob = nuke.Text_Knob(knob.name(), label)
    new_knob.fromScript(knob.toScript())

    return new_knob


def duplicate_int_knob(knob, label=None):
    """this_is_a_one_liner."""
    if label == None:
        label = explicit_label(knob)
    new_knob = nuke.Int_Knob(knob.name(), label)
    new_knob.fromScript(knob.toScript())

    return new_knob


def duplicate_format_knob(knob, label=None):
    """this_is_a_one_liner."""
    if label == None:
        label = explicit_label(knob)
    new_knob = nuke.Format_Knob(knob.name(), label)
    new_knob.fromScript(knob.toScript())

    return new_knob


def duplicate_float_knob(knob, label=None):
    """this_is_a_one_liner."""
    if label == None:
        label = explicit_label(knob)
    new_knob = nuke.Float_Knob(knob.name(), label)
    new_knob.fromScript(knob.toScript())
