"""this_is_a_one_liner."""
import re
import os
import types
import json
import nuke
import nukescripts
import kine.duplicate_knob as dk
import kine.common as common

class MasterKnobGroup(object):

    """this_is_a_one_liner."""

    def __init__(self, knob):
        """this_is_a_one_liner."""
        active_knob_name = "%s_ckActivate" % knob.name()
        self._active_knob = nuke.Boolean_Knob(active_knob_name, "------", 1)
        self._active_knob.setFlag(nuke.STARTLINE)

        dup_method = "duplicate_%s" % knob.Class().lower()
        self._knob = getattr(dk, dup_method)(knob)
        self._knob.clearFlag(nuke.STARTLINE)

    @property
    def active_knob(self):
        """this_is_a_one_liner."""
        return self._active_knob

    @property
    def knob(self):
        """this_is_a_one_liner."""
        return self._knob


class PropagateKnobsPanel(nukescripts.PythonPanel):

    """this_is_a_one_liner."""

    def __init__(self):

        nukescripts.PythonPanel.__init__(self, 'Propagate Knobs')
        self._source_node = common.get_source_node()
        self._source_knobs = self._source_node.knobs()
        self._knob_packs = {}
        self._knob_pack_array = []
        self._all_active_knob = None
        self._selection_knob = None
        self._go_button = None
        self._create_knob_packs()
        self.build_ui()
        self.show()
        
    def build_header(self):
        """this_is_a_one_liner."""

        title_text_knob = nuke.Text_Knob('title_text', '')
        txt = 'Propagating Knob Values for type: "%s" based on %s' % (self._source_node.Class(), self._source_node.name())
        title_text_knob.setValue(txt)
        title_text_knob.setFlag(nuke.STARTLINE)

        self._all_active_knob = nuke.Boolean_Knob(
            'all_active_knob', "All", 1)
        # self._all_active_knob.setFlag(nuke.STARTLINE)

        self._selection_knob = nuke.Enumeration_Knob(
            "selected_or_all", "Scope", common.PROPAGATION_SCOPE)
        self._selection_knob.setFlag(nuke.STARTLINE)

        self._enum_method_knob = nuke.Enumeration_Knob(
            "enum_method", "Non-matching Pulldown Fields", common.ENUM_POLICY)
        self._selection_knob.setFlag(nuke.STARTLINE)

        self._verbosity_knob = nuke.Enumeration_Knob(
            "verbosity", "Verbosity", common.VERBOSITY_LEVELS)
        self._verbosity_knob.setFlag(nuke.STARTLINE)


        self.addKnob(title_text_knob)
        self.addKnob(nuke.Text_Knob('divider', ''))
        self.addKnob(self._selection_knob)
        self.addKnob(self._verbosity_knob)
        self.addKnob(self._enum_method_knob)
        self.addKnob(nuke.Text_Knob('divider', ''))
        self.addKnob(self._all_active_knob)

    def build_ui(self):
        """this_is_a_one_liner."""

        self.build_header()

        for item in self._knob_pack_array:
            if isinstance(item, MasterKnobGroup):
                self.addKnob(item.active_knob)
                self.addKnob(item.knob)
            else:
                text_knob = nuke.Text_Knob("%s_text_spacer" % item, item)
                text_knob.setFlag(nuke.STARTLINE)
                self.addKnob(text_knob)

        # for key in self._knob_packs:
        #     grp = self._knob_packs[key]
        self._go_button = nuke.PyScript_Knob('do_propagate_knobs', 'Propagate')
        self._go_button.setFlag(nuke.STARTLINE)
        self.addKnob(self._go_button)
        # self.show()


    def _append_knob_pack(self, key):
        """this_is_a_one_liner."""
        if key in self._source_knobs:
            knob_object = self._source_knobs[key]
            master_grp = MasterKnobGroup(knob_object)
            self._knob_pack_array.append(master_grp)
            self._knob_packs[knob_object.name()] = master_grp

    def _append_text(self, text):
        """this_is_a_one_liner."""
        self._knob_pack_array.append(text)

    def _create_knob_packs(self):
        """this_is_a_one_liner."""
        # knobs = self._source_node.knobs()
        # source_klass = self._source_node.Class()

        filename = os.path.join(os.path.dirname(__file__),
                                'json', "%s.json" % self._source_node.Class())

        if os.path.isfile(filename):
            json_data = open(filename, "r")
            template = json.load(json_data)
        else:
            template = []
            for key in self._source_knobs:
                if self._source_knobs[key].Class() in common.ALLOWABLE_TYPES:
                    template.append(key)

        for item in template:
            if isinstance(item, types.StringTypes):
                self._append_knob_pack(item)
            else:
                for key, array in item.iteritems():
                    self._append_text(key)
                    for elem in array:
                        self._append_knob_pack(elem)

        # for knob_key in knobs:
        #     knob_object = knobs[knob_key]
        #     klass = knob_object.Class()
        # name = knob_object.name()
        #     if klass in common.ALLOWABLE_TYPES and knob_object.visible():
        #         self._knob_packs[name] = MasterKnobGroup(knob_object)

    def knobChanged(self, changing_knob):
        """manage enable/disable state."""
        regex = re.compile(r'^([a-z][a-zA-Z0-9_]+)_ckActivate')
        match = regex.match(changing_knob.name())
        if match:
            key = match.group(1)
            self._knob_packs[key].knob.setEnabled(changing_knob.value())
            return

        if changing_knob == self._all_active_knob:
            val = changing_knob.value()
            for key in self._knob_packs:
                self._knob_packs[key].knob.setEnabled(val)
                self._knob_packs[key].active_knob.setValue(val)
            return

        if changing_knob == self._go_button:
            self.do_propagate_knobs()

    def do_propagate_knobs(self):
        """this_is_a_one_liner."""
        enum_policy = common.ENUM_POLICY.index(self._enum_method_knob.value())
        verbosity = common.VERBOSITY_LEVELS.index(self._verbosity_knob.value())

        slaves = self.get_slave_nodes()
        active_knob_packs = [(key, pack) for key, pack in self._knob_packs.iteritems() if pack.active_knob.value()]

        if verbosity > 0:
            print 'Propagating node type "%s" based on %s ' % (self._source_node.Class(), self._source_node.name()) + "-" * 40
        if verbosity == 1:
            for key, knob_pack in active_knob_packs:
                 print key
            print "Propagating to nodes: " + "-" * 40
            for slave in slaves:
                print "target %s " % slave.name()

        for slave in slaves:
            slave_knobs = slave.knobs()
            for key, knob_pack in active_knob_packs:
                if key in slave_knobs:

                    if knob_pack.knob.Class() == "Enumeration_Knob":
                        mismatch = knob_pack.knob.value() not in slave_knobs[key].values()
                        if mismatch:
                            if enum_policy == 2: # force add value to list
                                if verbosity == 2:
                                    print "FORCE: %s.%s new key %s" % (slave.name(), key, knob_pack.knob.value()) 
                                new_values = slave_knobs[key].values() + [knob_pack.knob.value()]
                                slave_knobs[key].setValues(new_values)
                            if  enum_policy > 0: # set closest if not exists
                                if verbosity == 2:
                                    print "%s --> %s.%s" % (knob_pack.knob.toScript(), slave.name(), key)
                                slave_knobs[key].fromScript(knob_pack.knob.toScript())
                                if enum_policy == 1:
                                    if verbosity == 2:
                                        print "CLOSEST: %s.%s has no key %s. Set to %s instead" % (knob_pack.knob.toScript(), slave.name(), key, slave_knobs[key].value())
                            else: # skip
                                if verbosity == 2:
                                    print "SKIP: %s.%s has no key %s" % (slave.name(), key, knob_pack.knob.value()) 
                        else: # no mismatch
                            if verbosity == 2:
                                print "%s --> %s.%s" % (knob_pack.knob.toScript(), slave.name(), key)
                            slave_knobs[key].fromScript(knob_pack.knob.toScript())
                    else: # not Enum
                        if verbosity == 2:
                            print "%s --> %s.%s" % (knob_pack.knob.toScript(), slave.name(), key)
                        slave_knobs[key].fromScript(knob_pack.knob.toScript())

        if verbosity > 0:
            print "-" * 80

    def get_slave_nodes(self):
        """this_is_a_one_liner."""
        klass = self._source_node.Class()
        if self._selection_knob.value() == "Selected":
            return nuke.selectedNodes(klass)
        else:
            return nuke.allNodes(klass)
