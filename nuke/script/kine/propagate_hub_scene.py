
import re
import os
import types
# import threading
# import time
import nuke
import nukescripts
import kine.duplicate_knob as dk
import kine.common as common
import mpc.nuke.hubNuke.hubNukeReadNode as hrn; 


SCENE_REGEX_STR = "([a-z][a-z0-9]+)_[a-zA-Z0-9_]+"

class PropagateHubSceneResultsPanel(nukescripts.PythonPanel):
    def __init__(self, title, text):
        nukescripts.PythonPanel.__init__(self, title)
        knob = nuke.Multiline_Eval_String_Knob('log_knob', '')
        knob.setValue(text)
        self.addKnob(knob)
        self.setMinimumSize(800, 300)
        self.show()


class PropagateHubScenePanel(nukescripts.PythonPanel):
    """Allow artist to propagate a change of scene in HubRead nodes.

    The HubRead node contains enumeration knobs for scene, shot,
    layer, version, and pass. The set of options for each are 
    dependent on the previous field's value. If scene is changed,
    the choice of shots will change. If shot is changed, the choice
    of layers will change and so on. 

    The Read node's filename fields and label will be generated from 
    these fields.

    When changing these knobs interactively, a callback is called
    to change the choice of values in dependent knobs. However - the 
    chosen value for those knobs will be set to the first value in 
    the set of options. When switching a scenefor a whole nuke script,
    it would be desirable for dependent knobs to be set to the equivalent
    value to the one they were at, only for the new scene. 
    For example, consider the node with following settings:
    Scene = uk_master
    Shot = uk040 
    Layer = E3D_uk040
    Version = 5

    If we change scene to aus_master we want shot to change to
    aus040 (not aus010) and we want layer to change to E3D_aus040
    (not I_aus040) and so on.

    This class takes care of reconfiguring all fields in the
    HubRead node in a cascading fashion which ultimately 
    results in the file paths being updated.
    """ 

    def __init__(self):
        """ Set up UI for hub scene propagation.

        Make pull down menus to specify from and to and scope, and
        a button to do the propagation.
        """
        nukescripts.PythonPanel.__init__(self, 'Propagate Hub Scene')
        self._source_node = common.get_source_node()

        if not self._source_node.knobs().has_key("Hub"):
            print "# Not a Hub Read Node"
            return

        scene_knob = self._source_node.knob("scene")
        self._from_scene_knob = dk.duplicate_enumeration_knob(scene_knob, 'From')
        self._to_scene_knob = dk.duplicate_enumeration_knob(scene_knob, 'To')

        self.addKnob(self._from_scene_knob)
        self.addKnob(self._to_scene_knob)

        # self._old_scene = scene_knob.value()
   
        # regex = re.compile(SCENE_REGEX_STR)
        # mat = re.match(regex, self._old_scene)
        # if not mat:
        #     print "# Can't use %s as old_scene doesn't match regex %s" % (self._source_node.name(), SCENE_REGEX_STR)
        #     return

        # self._old_scene_major = mat.groups()[0]

        self._selection_knob = nuke.Enumeration_Knob(
            "selected_or_all", "Scope", common.PROPAGATION_SCOPE)
        self._selection_knob.setFlag(nuke.STARTLINE)
        self.addKnob(self._selection_knob)

        self._go_button = nuke.PyScript_Knob('go_but', 'Propagate')
        self._go_button.setFlag(nuke.STARTLINE)
        self.addKnob(self._go_button)
        self._log = []
        self.show()

    def get_slave_nodes(self):
        """Get selected or all Hub Read nodes.
        """
        nodes = []
        if self._selection_knob.value() == "Selected":
            nodes = nuke.selectedNodes("Read")
        else:
            nodes = nuke.allNodes("Read")
        
        return [node for node in nodes if node.knobs().has_key("Hub")]


    def knobChanged(self, changing_knob):
        """do it."""

        if changing_knob == self._go_button:
            self.do_propagate_hub_scene()

            text = '\n'.join(self._log)
            panel = PropagateHubSceneResultsPanel('Propagate Hub Scene: Log', text)

            # nuke.display()
            # p = nuke.Panel('Propagate Hub Scene: Log')
            # p.addNotepad('', text)
            # p.show()

        # threading.Thread(target=self.do_propagate_hub_scene).start() 

        # if changing_knob == self._print_button:
        #     self.do_propagate_hub_scene(True)




    def do_propagate_hub_scene(self):
        """Do the propagation"""

        self._log = []

        old_scene = self._from_scene_knob.value()
        new_scene = self._to_scene_knob.value()
        if new_scene == old_scene:
            self._log.append('SKIP ALL NODES: new_scene "%s" is the same as old_scene "%s"' % (new_scene, old_scene))
            return
            
        regex = re.compile(SCENE_REGEX_STR)
        mat = re.match(regex, new_scene)
        if not mat:
            self._log.append("SKIP ALL NODES: New_scene %s doesn't match regex %s" % (new_scene, SCENE_REGEX_STR))
            return
        new_scene_major = mat.groups()[0]

        mat = re.match(regex, old_scene)
        if not mat:
            self._log.append("SKIP ALL NODES: old_scene %s doesn't match regex %s" % (old_scene, SCENE_REGEX_STR))
            return
        old_scene_major = mat.groups()[0]

        self._log.append('Changing from "%s" to "%s" for "%s" HubRead nodes' % (old_scene, new_scene,  self._selection_knob.value()))
        self._log.append('-' * 80)
        slaves = self.get_slave_nodes()   

        # set max panels to 1 so that we dont end up
        # filling the bin. It will be reset later 
        maxPanelsKnob = nuke.toNode('preferences')['maxPanels']
        maxPanelsValue = maxPanelsKnob.value()
        maxPanelsKnob.setValue(1)

        progress = nuke.ProgressTask("Switch Hub Scene")
        progress.setMessage("Propagating...")

        i = 0
        progressInc = 100.0 /  len(slaves)
        for node in slaves:
            if progress.isCancelled():
                self._log.append("ABORT BY USER :")
                break;
            progress.setProgress( int(i * progressInc) )
            progress.setMessage(node.name())
            i += 1

            # scene
            scene = node.knob("scene").value()
            if scene != old_scene:
                self._log.append('--------------- "%s". SKIPPED: Scene value: "%s" is not "%s"' % (node.name(), scene, old_scene))
                continue

            # shot
            shot = node.knob("shot").value()
            regexstr = "%s_([0-9]+)" % old_scene_major
            regex = re.compile(regexstr)
            mat = re.match(regex, shot)
            if not mat:
                self._log.append('--------------- "%s". SKIPPED: Shot "%s" doesnt match regex "%s"' % (node.name(), shot, regexstr))
                continue
            shot_number = mat.groups()[0]

            # layer
            layer = node.knob("layer").value()
            regexstr = "([A-Z0-9]+)_%s([a-zA-Z0-9_]*)" % shot
            regex = re.compile(regexstr)
            mat = re.match(regex, layer)
            if not mat:
                self._log.append('--------------- "%s". SKIPPED: Layer "%s" doesnt match regex "%s"' % (node.name(), layer, regexstr))
                continue
            layer_type = mat.groups()[0]
            layer_minor = mat.groups()[1] or ''

            # version
            layer_version = node.knob("layerVersion").value()

            # pass
            new_pass = node.knob("pass").value()
            
            new_shot = "%s_%s" % (new_scene_major, shot_number)
            new_layer = "%s_%s%s" % (layer_type, new_shot, layer_minor)

            fail = self.update_node(node,  new_scene, new_shot, new_layer, new_pass)

            if fail:
                fail = self.update_node(node, scene, shot, layer, new_pass, layer_version)
                if fail:
                    self._log.append('NODE COULD NOT BE REPAIRED: "%s" ----------------------' % node.name())   



        maxPanelsKnob.setValue(maxPanelsValue) 
        del(progress)


    def update_node(self, node, new_scene, new_shot, new_layer, new_pass, layer_version=-1):
        """
        Attempt to configure the node's hub tab to given values.

        For each field, we set the value and then make a hub update
        call, which in turn configures the options for the next field.

        There are two modes: normal (setting new values), or repairing 
        after a failed attempt. We derive repair mode from the value of
        layer_version. If it is -1, we are in normal mode.

        In normal mode, if a problem is encountered then  this method
        will be called again in repair mode with original values to undo
        any damage.

        Layer version is set to the latest available version in normal
        mode, and to the original version in repair mode. 
        """



        nuke.show(node)
        repairing = (layer_version != -1)

        if repairing:
            self._log.append('--------------- "%s" ------------------ REPAIR' % node.name())   
        else:    
            self._log.append('--------------- "%s" ------------------' % node.name())
        k = node.knob("scene")
        k.setValue(new_scene)
        self._log.append('SCENE SET     : "%s.scene" = "%s"' % (node.name(), new_scene))
        hrn.refreshNode(node)


        k = node.knob("shot")
        if (not new_shot) or( new_shot not in k.values()):
            self._log.append('SHOT ABORT    : "%s": Cant find "%s" in "%s"' % (node.name(), new_shot, new_scene))
            return 1

        k.setValue(new_shot)
        self._log.append('SHOT SET      : "%s.shot" = "%s"' % (node.name(), new_shot))
        hrn.refreshNode(node)

        # If layerVersion is the only thing to 
        # have changed when hubRead.refresh() runs, the existing layer is
        # considered relevant and is appended to the new list of layers.
        # To avoid this behavior, we set layerVersion to 1 here so it is 
        # seen to have changed in the same refresh op as layer below.
        node.knob("layerVersion").setValue(1)



        k = node.knob("layer")
        if (not new_layer) or (new_layer not in k.values()):
            self._log.append('LAYER ABORT   : "%s": Cant find "%s" in "%s"' % (node.name(), new_layer, new_shot))
            return  1
        k.setValue(new_layer)
        self._log.append('LAYER SET     : "%s.layer" = "%s"' % (node.name(), new_layer))
        hrn.refreshNode(node)


        k = node.knob("layerVersion")
        if not repairing:
            # raw_versions = ", ".join(k.values())
            # possible_versions = sorted([int(x) for x in k.values()])
            # versions have been sorted in reverse, so latest is [0]
            layer_version = k.values()[0]
            # self._log.append('RAW VERSIONS: "%s"' % raw_versions)
        # else:
            # self._log.append('REPAIR LAYER VERSION BACK TO %s' % layer_version)
        k.setValue(layer_version)
        self._log.append('VERSION SET   : "%s.layerVersion" = "%s"' % (node.name(), layer_version))
        hrn.refreshNode(node)


        k = node.knob("pass")
        if (not new_pass) or (new_pass not in k.values()):
            self._log.append('PASS ABORT    : "%s": Cant find "%s"' % (node.name(), new_pass))
            return 1
        k.setValue(new_pass)
        self._log.append('PASS SET   : "%s.pass" = "%s"' % (node.name(), new_pass))


        # one for luck
        hrn.refreshNode(node)


        tab = hrn.getTab(node)
        tab.loadImages()
        if repairing:
            self._log.append('NODE REPAIRED: "%s" ----------------------' % node.name() )  
        else:    
            self._log.append('NODE CONVERTED: "%s" ----------------------' % node.name())   
        return 0 

# nuke /jobs/bet365Sportsbook1415_1415067/uk001a_Welcome_Master/uk001a_030/nuke/comp/scene/software/testingSceneforHubRead_v002.nk

# nuke.toNode("HubRead44").knob("layerVersion").values()

